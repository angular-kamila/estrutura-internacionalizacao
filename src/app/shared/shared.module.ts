import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AccessibilityBarComponent } from './components/accessibility-bar/accessibility-bar.component';
import { AlertComponent } from './components/alert/alert/alert.component';
import { FooterComponent } from './components/footer/footer.component';
import { HeaderComponent } from './components/header/header.component';
import { TranslateModule } from '@ngx-translate/core';
import { CollapseModule } from 'ngx-bootstrap/collapse';



@NgModule({
  declarations: [
    HeaderComponent,
    AccessibilityBarComponent,
    FooterComponent,
    AlertComponent
  ],
  exports: [
    HeaderComponent,
    AccessibilityBarComponent,
    FooterComponent,
    AlertComponent
  ],
  imports: [
    CommonModule,
    TranslateModule,
    CollapseModule.forRoot()
  ]
})
export class SharedModule { }
