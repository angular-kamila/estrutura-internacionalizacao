export enum UserPreferencesKeys {
    highContrast = 'highContrast',
    language = 'language',
    fontSize = 'fontSize',
    rememberMe = 'rememberMe',
}