export class Alert {
    id: string;
    title: string;
    message: string;
    type?: string;
    icon?: string;
    autoClose: boolean;
    keepAfterRouteChange: boolean;

    constructor(init?: Partial<Alert>) {
        Object.assign(this, init);
    }
}