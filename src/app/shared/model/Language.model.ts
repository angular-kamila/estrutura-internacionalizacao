export class Language {

    public id: number;
    public description: string;
    public isActive: boolean;
    public tag: string;
    public icon: string;
    
}