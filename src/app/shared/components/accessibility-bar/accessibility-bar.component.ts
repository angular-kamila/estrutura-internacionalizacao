import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Observable } from 'rxjs';
import { ThemeService } from 'src/app/services/theme.service';
import { UserPreferencesKeys } from '../../constants/userPreferencesKeys.enum';

@Component({
  selector: 'app-accessibility-bar',
  templateUrl: './accessibility-bar.component.html',
  styleUrls: ['./accessibility-bar.component.scss']
})
export class AccessibilityBarComponent implements OnInit {

  isHighContrastTheme: Observable<boolean>;
  isHighContrast = true;

  constructor(
    public translate: TranslateService,
    private themeService: ThemeService
  ) { }

  ngOnInit(): void {
    this.isHighContrastTheme = this.themeService.isHighContrastTheme;
    this.isHighContrastTheme.subscribe((value: boolean) => {
      this.isHighContrast = value;
    });
  }

  setLanguage(language: string): void {
    this.translate.use(language);
  }

  toogleHighContrastTheme(): void {
    this.isHighContrast = !this.isHighContrast;
    this.themeService.setHighContrastTheme(this.isHighContrast);
    localStorage.setItem(UserPreferencesKeys.highContrast, this.isHighContrast.toString());
  }

}
