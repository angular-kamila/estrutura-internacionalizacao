import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { filter } from 'rxjs/operators';
import { Alert } from '../../model/Alert.model';

@Injectable({
  providedIn: 'root'
})
export class AlertService {

  private subject = new Subject<Alert>();

  private defaultOptions: Alert = {
    id: 'default-alert',
    message: '',
    title: '',
    autoClose: true,
    keepAfterRouteChange: true
  };

  onAlert(id = this.defaultOptions.id): Observable<Alert> {
    return this.subject.asObservable().pipe(filter((alert) => alert && alert.id === id));
  }

  success(alertOptions?: any) {
    this.alert(new Alert({ ...alertOptions, type: 'success', icon: 'check' }));
  }

  error(alertOptions?: any) {
    this.alert(new Alert({ ...alertOptions, type: 'danger', icon: 'alert-error' }));
  }

  info(alertOptions?: any) {
    this.alert(new Alert({ ...alertOptions, type: 'info', icon: 'alert-info-cir' }));
  }

  warn(alertOptions?: any) {
    this.alert(new Alert({ ...alertOptions, type: 'warning', icon: 'alert-notice' }));
  }

  alert(alert: Alert) {
    this._checkDefaultOptions(alert);
    this.subject.next(alert);
  }

  clear(id = this.defaultOptions.id): void {
    this.subject.next(new Alert({ id }));
  }

  /**
   * Insere atributos default caso não estejam presentes no objeto recebido
   */
  private _checkDefaultOptions(obj) {
    if (obj) {
      for (const key in this.defaultOptions) {
        if (!obj.hasOwnProperty(key)) {
          obj[key] = this.defaultOptions[key];
        }
      }
    }
  }

}