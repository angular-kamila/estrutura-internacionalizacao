import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './security/auth/auth.guard';
import { LoginComponent } from './security/login/login/login.component';
import { ContainerComponent } from './view/container/container.component';
import { HomeComponent } from './view/home/home.component';


const routes: Routes = [
  {
    path: '',
    component: ContainerComponent,
    children: [
      { path: '', component: HomeComponent, data: { title: 'HOME.BROWSER_TITLE'} }
    ]
  },
  {
    path: 'login',
    component: LoginComponent,
    data: { title: 'LOGIN.BROWSER_TITLE' }
  }
 /* Private route
  {
    path: 'portal',
    component: ContainerAdminComponent,
    canActivate: [AuthGuard],
    canActivateChild: [AuthGuard],
    children: [
      { path: '', component: HomeAdminComponent }
    ]
  }
  */
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [AuthGuard]
})
export class AppRoutingModule { }
