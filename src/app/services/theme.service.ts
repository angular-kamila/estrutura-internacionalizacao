import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { UserPreferencesKeys } from '../shared/constants/userPreferencesKeys.enum';

@Injectable({
  providedIn: 'root'
})
export class ThemeService {

  isHighContrastTheme: Observable<boolean>;
  private highContrastTheme: BehaviorSubject<boolean>;

  constructor() {
    this.initThemeService();
  }

  setHighContrastTheme(isHighContrastTheme: boolean): void {
    this.highContrastTheme.next(isHighContrastTheme);
  }

  private initThemeService(): void {
    const preferencesValue = JSON.parse(localStorage.getItem(UserPreferencesKeys.highContrast));
    this.highContrastTheme = new BehaviorSubject<boolean>(preferencesValue);
    this.isHighContrastTheme = this.highContrastTheme.asObservable();
  }

}

