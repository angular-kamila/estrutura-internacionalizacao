import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  apiUrl: string;

  constructor(private http: HttpClient) {
    this.apiUrl = environment.apiUrl;
  }

  get(path: string, params: HttpParams = new HttpParams()): Observable<any> {
    return this.http.get(this.apiUrl + `${path}`,
      { headers: this._setHeaders(), params: params });
  }

  getParams(path: string, obj: any) {
    const params = this._getParams(obj);
    return this.http.get(this.apiUrl + `${path}` + '?' + params,
      { headers: this._setHeaders() });
  }

  put(path: string, body: Object = {}): Observable<any> {
    return this.http.put(this.apiUrl + `${path}`, body,
      { headers: this._setHeaders() }
    );
  }

  post(path: string, body: Object = {}): Observable<any> {
    return this.http.post(this.apiUrl + `${path}`, body,
      { headers: this._setHeaders() }
    );
  }

  delete(path): Observable<any> {
    return this.http.delete(
      this.apiUrl + `${path}`,
      { headers: this._setHeaders() }
    );
  }

  private _setHeaders(): HttpHeaders {
    const accessToken = localStorage.getItem('token');
    const headersConfig = {
      'Content-Type': 'application/json',
      'Accept': 'application/json, */*',
      'Authentication': `Bearer ${accessToken}`
    };
    return new HttpHeaders(headersConfig);
  }

  private _setHeadersPdf(): HttpHeaders {
    const accessToken = localStorage.getItem('token');
    const headersConfig = {
      'Content-Type': 'application/pdf',
      'Accept': 'application/pdf',
      'Authentication': `Bearer ${accessToken}`
    };
    return new HttpHeaders(headersConfig);
  }

  private _getParams(obj): string {
    let queryParams = '';
    for (const key in obj) {
      if (obj.hasOwnProperty(key)) {
        if (obj[key] != null) {
          queryParams += key + '=' + obj[key] + '&';
        }
      }
    }
    return queryParams;
  }

}

