import { Component } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { ThemeService } from './services/theme.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  isHighContrastTheme: Observable<boolean>;

  constructor(
    private themeService: ThemeService,
    private titleService: Title,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private translateService: TranslateService) { }

  ngOnInit(): void {
    this.isHighContrastTheme = this.themeService.isHighContrastTheme;

    this.setPageTitle();

    this.translateService.onLangChange.subscribe(res => {
      this.updatePageTitle();
    });
  }

  private setPageTitle() {
    const appTitle = this.titleService.getTitle();

    this.router.events.pipe(
      filter(event => event instanceof NavigationEnd),
      map(() => {
        let child = this.activatedRoute.firstChild;
        while (child.firstChild) {
          child = child.firstChild;
        }
        if (child.snapshot.data['title']) {
          return child.snapshot.data.title;
        } else {
          return appTitle;
        }
      })
    ).subscribe((title: string) => {
      this.translateService.get(title)
        .subscribe(res => this.titleService.setTitle(res));
    });
  }

  private updatePageTitle() {
    this.activatedRoute.url.subscribe(() => {
      let child = this.activatedRoute.firstChild;
      while (child.firstChild) {
        child = child.firstChild;
      }
      if (child.snapshot.data['title']) {
        this.translateService.get(child.snapshot.data.title)
          .subscribe(title => this.titleService.setTitle(title));
      };
    });
  }

}

